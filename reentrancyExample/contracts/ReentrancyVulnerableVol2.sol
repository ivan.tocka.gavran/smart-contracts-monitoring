pragma solidity ^0.4.4;
import './KeepMyMoneySafeSpec.sol';



// copy of ReentrancyVulnerable contract. (the copy exists since for now contracts and specs are tightly bound so the
// functions this contract invokes are from the specific other contract)
// in this spec it will try to guarantee to its users that it always has enough money to give back to them 
// (its promised property)
// the core example (function withdrawBalance) taken from https://github.com/ConsenSys/smart-contract-best-practices#race-conditions
// this contract stores the money for users and upon their request, returns it back to them (without taking any 
// provision for itself)

contract ReentrancyVulnerableVol2{
	mapping (address => uint) private userBalances;
	bool public useChecker;
	event ContractCreated();
	event RuntimeMonitoringStoppedExecution();
	KeepMyMoneySafeSpec public checker;
	
	function ReentrancyVulnerableVol2(bool activateCheckerUsage)
	{

		ContractCreated();
		useChecker = activateCheckerUsage;
		if (useChecker == true){
			checker = new KeepMyMoneySafeSpec();
		}
	}
	
	
	function storeMoneySafelyAtContract() payable
	{
		// putting in before the actual order counting on that any future exceptions will revert everything
		// in reality external service would have to watch to see whether a transaction actually happened
		if (useChecker == true){
			checker.moneyStored(msg.sender, msg.value);
		}
		userBalances[msg.sender] = msg.value;
		
	}
	
	function getStoredMoney(address user) public returns (uint balance)
	{
		return userBalances[user];
	}
	
	function withdrawBalance() public 
	{
		
		
			
	    uint amountToWithdraw = userBalances[msg.sender];
	    if (useChecker == true){
			checker.moneyToWithdraw(msg.sender, amountToWithdraw);
		}
	    if (useChecker == true){
	    	if (checker.isItOK() == false){
	    		RuntimeMonitoringStoppedExecution();
	    		revert();
	    	}
	    }
	    if (!(msg.sender.call.value(amountToWithdraw)())) { revert(); } // At this point, the caller's code is executed, and can call withdrawBalance again
	    if (useChecker == true){
			checker.moneyWithdrawn(msg.sender, amountToWithdraw);
		}
	    userBalances[msg.sender] = 0;
	}
}