pragma solidity ^0.4.4;
import './Specification.sol';

//this is a simple checker contract illustrating checking of the reentrancy pattern for a particular function
// (the function is not encoded in the arguments, but this should also be possible)

contract ReentrancyChecker is Specification
{
	
	
	int public numberOfEntriesIntoFunction = 0;
	
	
	function enteringFunction() 
	{
		numberOfEntriesIntoFunction += 1;
	}
	
	function exitingFunction()
	{
		numberOfEntriesIntoFunction -= 1;
		
	}
	
	function isItOK() returns (bool ok)
	{
		if (numberOfEntriesIntoFunction > 1)
			return false;
		else
			return true;
	}
	
		
}
