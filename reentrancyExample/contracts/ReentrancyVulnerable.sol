pragma solidity ^0.4.4;
import './ReentrancyChecker.sol';



//the core example (function withdrawBalance) taken from https://github.com/ConsenSys/smart-contract-best-practices#race-conditions
//this contract stores the money for users and upon their request, return it back to them (without taking any 
//provision for itself)
contract ReentrancyVulnerable{
	mapping (address => uint) private userBalances;
	bool public useChecker;
	event ContractCreated();
	event RuntimeMonitoringStoppedExecution();
	ReentrancyChecker public checker;
	
	function ReentrancyVulnerable(bool activateCheckerUsage)
	{

		ContractCreated();
		useChecker = activateCheckerUsage;
		if (useChecker == true){
			checker = new ReentrancyChecker();
		}
	}
	
	
	function storeMoneySafelyAtContract() payable
	{
		userBalances[msg.sender] = msg.value;
	}
	
	function getStoredMoney(address user) public returns (uint balance)
	{
		return userBalances[user];
	}
	
	function withdrawBalance() public 
	{
		
		if (useChecker == true){
			checker.enteringFunction();
		}
			
	    uint amountToWithdraw = userBalances[msg.sender];
	    if (useChecker == true){
	    	if (checker.isItOK() == false){
	    		RuntimeMonitoringStoppedExecution();
	    		revert();
	    	}
	    }
	    if (!(msg.sender.call.value(amountToWithdraw)())) { revert(); } // At this point, the caller's code is executed, and can call withdrawBalance again
	    userBalances[msg.sender] = 0;
	    
	    if(useChecker == true){
	    	checker.exitingFunction();
	    }
	}
}