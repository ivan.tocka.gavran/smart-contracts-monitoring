var ReentrancyVulnerableContract = artifacts.require("./ReentrancyVulnerable.sol");


contract('ReentrancyVulnerable', function(accounts) {
  
   // check whether the contract upon deployment has the right (0) value per some non-specific user
  it("testing the initial deploymend parameters", function() {
	var setCheckerUsage = true;
    return ReentrancyVulnerableContract.new(setCheckerUsage)
    .then(function(instance) {
      return instance.getStoredMoney.call(accounts[0])
      .then(function(amountOfMoneyStored) {
    	  console.log("initial amount of money = "+amountOfMoneyStored.valueOf());
    	  assert.equal(amountOfMoneyStored.valueOf(), 0, "0 wasn't the initial threshold");
      })
      .then(function(){
    	 return instance.useChecker.call()
    	 .then(function(usingChecker){
    		 console.log("using checker = "+usingChecker);
    		 assert.equal(usingChecker.valueOf(), setCheckerUsage, "using checker was not initialized to true");
    	 });
      });
    });
  });
  
  
  it("testing sending some money to the contract", function(){
	 var setCheckerUsage = true;
	 return ReentrancyVulnerableContract.new(setCheckerUsage)
	 .then(function(instance){
		var moneyToStore = web3.toWei(5, "ether");
		return instance.storeMoneySafelyAtContract({from:accounts[0], to: instance.address, value: moneyToStore})
		.then(function(){
			console.log("some ether sent");
			return instance.getStoredMoney.call(accounts[0])
			.then(function(amountOfMoneyStored){
				console.log("contract is currently storing "+amountOfMoneyStored.valueOf()+" at wanted address");
				assert.equal(amountOfMoneyStored.valueOf(), moneyToStore, "contract did not right amount of ether");
				//assert.equal(amountOfMoneyStored.valueOf(), web3.toWei(3, "ether"), "contract did not right amount of ether");
			});
		});
	 });
  });
  
  
  
	

});


