var ReentrancyVulnerableContract = artifacts.require("./ReentrancyVulnerable.sol");
var ReentrancyVulnerableContractVol2 = artifacts.require("./ReentrancyVulnerableVol2.sol");
var AttackerContract = artifacts.require("./AttackerContract.sol")
var AttackerContractVol2 = artifacts.require("./AttackerContractVol2.sol")

/*
 * test scenario: 
 * there is a contracat vulnerable to reentrancy attack (vulnerableInstance). It promises to store money of other accounts and upon request return it to them.
 * there is an attacker (attacker instance) that puts its initial deposit to vulnerableInstance, waits for others to put some deposits and then takes advantage of
 * reentrancy vulnerability to get the money of others.
 * However, there is a checker contract that checks for reentrancies and enables vulnerable contract to revert the state if that occurs.
 * 
 * variables to modify for different scenarios
 *   - setCheckerUsage: if true, checker will be used, otherwise not.
 *   - numberOfReentrancies: how many times to try to enter. this should also be carefully chosen. if 0, nothing will happen (attacker will get only its money back)
 *                           however, if too big (if the vulnerableInstance doesn't have enough money or the gas provided by attacker is not enough), everything will be reverted     
 */
contract('ReentrancyVulnerable', function(accounts) {
  
   // check whether the contract upon deployment has the right (0) value per some non-specific user
  var vulnerableInstance;
  var attackerInstance;
  
  
  
//  it("testing withdraw function invoked from (potentially malicious) account", function(){
//	  var setCheckerUsage = true;
//	  
//	  var numberOfReentrancies = 0;
//	  if(setCheckerUsage == true)
//		  console.log("checker is used");
//	  else
//		  console.log("checker not used");
//	  console.log("==========\n\n\n");
//	  var attackersInitialDonation = web3.toWei(5, "ether");
//	  return ReentrancyVulnerableContract.new(setCheckerUsage)
//	  .then(function(instance){
//		  vulnerableInstance = instance;
//		  return AttackerContract.new(numberOfReentrancies, instance.address)
//	  })
//	  .then(function(instance){
//		  var donationToAttacker = web3.toWei(10, "ether");
//		  attackerInstance = instance;
//		  return attackerInstance.getSomeDonations({from:accounts[1], to: attackerInstance.address, value: donationToAttacker})
//	  })
//	  .then(function(){
//		  return attackerInstance.storeMoney(attackersInitialDonation);
//	  })
//	  .then(function(){
//		  console.log("attacker stored "+attackersInitialDonation+" to the vulnerable contract as its initial deposit");
//		  console.log("==========\n");
//		  return vulnerableInstance.getStoredMoney.call(attackerInstance.address)
//	  })
//	  .then(function(amount){
//		  console.log("amount of ether stored at vulnerable contract for attacker's account "+amount.valueOf());
//		  console.log("ether that the attacker currently possesses is "+web3.eth.getBalance(attackerInstance.address.valueOf()));
//		  console.log("==========\n");
//		  var moneyToStore = web3.toWei(15, "ether");
//		  return vulnerableInstance.storeMoneySafelyAtContract({from:accounts[0], to: vulnerableInstance.address, value: moneyToStore})
//	  })
//	  .then(function(){
//		 console.log("another external account just stored some additional ethere at vulnerable contract");
//		 console.log("ether at account 0: "+web3.eth.getBalance(accounts[0]));
//		 console.log("ether at vulnerable contract: "+web3.eth.getBalance(vulnerableInstance.address));
//		 console.log("ether at attacker: "+web3.eth.getBalance(attackerInstance.address));
//		 console.log("==========\n");
//		 return attackerInstance.requestYourBalance();
//	  })
//	  .then(function(){
//		  console.log("attacker asked to get his money back");
//		  console.log("ether at account 0: "+web3.eth.getBalance(accounts[0]));
//		  console.log("ether at vulnerable contract: "+web3.eth.getBalance(vulnerableInstance.address));
//		  console.log("ether at attacker: "+web3.eth.getBalance(attackerInstance.address));
//		  console.log("==========\n");
//		  
//	  });
//	  
//	  
//
//  });
//
//  
//  
//  
//  
//
//  it("testing that after revert all the values are as they should be", function(){
//	 console.log("ether at account 0: "+web3.eth.getBalance(accounts[0]));
//	 console.log("ether at vulnerable contract: "+web3.eth.getBalance(vulnerableInstance.address));
//	 console.log("ether at attacker: "+web3.eth.getBalance(attackerInstance.address));
//	 console.log("===========")
//	 return vulnerableInstance.getStoredMoney.call(attackerInstance.address)
//	 .then(function(amount){
//		 console.log("amount of ether stored at vulnerable contract for attacker's account is "+amount); 
//	 });
//  });
//  
  
  // testing that the behavior is right also for a spec that says ("all my money at one place")
  it("testing withdraw function invoked from (potentially malicious) account", function(){
	  var setCheckerUsage = true;
	  
	  var numberOfReentrancies = 2;
	  if(setCheckerUsage == true)
		  console.log("checker is used");
	  else
		  console.log("checker not used");
	  console.log("==========\n\n\n");
	  var attackersInitialDonation = web3.toWei(5, "ether");
	  return ReentrancyVulnerableContractVol2.new(setCheckerUsage)
	  .then(function(instance){
		  vulnerableInstance = instance;
		  return AttackerContractVol2.new(numberOfReentrancies, instance.address)
	  })
	  .then(function(instance){
		  var donationToAttacker = web3.toWei(10, "ether");
		  attackerInstance = instance;
		  console.log("attacker instance is "+attackerInstance.address);
		  return attackerInstance.getSomeDonations({from:accounts[1], to: attackerInstance.address, value: donationToAttacker})
	  })
	  .then(function(){
		  console.log("attacker has some money to play with");
		  return attackerInstance.storeMoney(attackersInitialDonation, {gas:800000});
	  })
	  .then(function(){
		  console.log("attacker stored "+attackersInitialDonation+" to the vulnerable contract as its initial deposit");
		  console.log("==========\n");
		  return vulnerableInstance.getStoredMoney.call(attackerInstance.address)
	  })
	  .then(function(amount){
		  console.log("amount of ether stored at vulnerable contract for attacker's account "+amount.valueOf());
		  console.log("ether that the attacker currently possesses is "+web3.eth.getBalance(attackerInstance.address.valueOf()));
		  console.log("==========\n");
		  var moneyToStore = web3.toWei(15, "ether");
		  return vulnerableInstance.storeMoneySafelyAtContract({from:accounts[0], to: vulnerableInstance.address, value: moneyToStore})
	  })
	  .then(function(){
		 console.log("another external account just stored some additional ethere at vulnerable contract");
		 console.log("ether at account 0: "+web3.eth.getBalance(accounts[0]));
		 console.log("ether at vulnerable contract: "+web3.eth.getBalance(vulnerableInstance.address));
		 console.log("ether at attacker: "+web3.eth.getBalance(attackerInstance.address));
		 console.log("==========\n");
		 return attackerInstance.requestYourBalance();
	  })
	  .then(function(){
		  console.log("attacker asked to get his money back");
		  console.log("ether at account 0: "+web3.eth.getBalance(accounts[0]));
		  console.log("ether at vulnerable contract: "+web3.eth.getBalance(vulnerableInstance.address));
		  console.log("ether at attacker: "+web3.eth.getBalance(attackerInstance.address));
		  console.log("==========\n");
		  
	  });
	  
	  

  });

  
});


