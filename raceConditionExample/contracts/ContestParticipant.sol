pragma solidity ^0.4.4; //although I don't have specific requirements, but compiler complains if the pragma is not specified
import './ContestOrganizer.sol';

contract ContestParticipant
{
	ContestOrganizer organizerContract;
	uint mySolution;
	event ReceivedMoney(uint value);
	event TakingPart();
	function() payable
	{
		ReceivedMoney(msg.value);
	}
	
	function ContestParticipant(address organizersAddress)
	{
		organizerContract = ContestOrganizer(organizersAddress);
	}
	
	
	
	function hardWork()
	{
		mySolution = 6;
	}
	
	function participate()
	{
		TakingPart();
		organizerContract.participateInContest(mySolution);
	}
	
	
	
}
