pragma solidity ^0.4.4;

/*
 * this is a spec for a KingOfEther contract and it corresponds to:
 * make sure that a king that came into that position by paying x ether will either rule forever or will be payed 1.5x - 0.01*1.5x:::no! this is actually not
 * the responsibility of the contract. The only thing the contract can guarantee is that it would send it... but what is the gain then? we are only verifying 
 * that the contract is not malicious in its logic (but this class of errors are still not there) 
 */

contract KoEKingsSpecification 
{
	
	
	
	address currentKing;
	uint currentClaimPrice;
	bool kingSittingOnTheThrone = false;
	bool kingReceivedHisMoney = true; //starts with true as the first king doesn't won't be payed out (first king is the wizard)
	uint wizardCommissionFractionNum;
    uint wizardCommissionFractionDen;
    event PayingTheOldKingEvent(uint pricePayed, uint fairPrice);
    event Coronation();
	
	function KoEKingsSpecification(uint _wizardCommissionFractionNum, uint _wizardCommissionFractionDen)
	{
		wizardCommissionFractionNum = _wizardCommissionFractionNum; 
		wizardCommissionFractionDen = _wizardCommissionFractionDen;
	}
	
	function newKingCoronated(address kingsAddress, uint priceForFutureUsurpers) 
	{
		Coronation();
		if (kingSittingOnTheThrone == false){
			kingSittingOnTheThrone = true;	
		}
		currentKing = kingsAddress;
		
		currentClaimPrice = priceForFutureUsurpers;
		kingReceivedHisMoney = false;
		
	}
	
	function payingTheOldKing(uint pricePayed)
	{
		PayingTheOldKingEvent(pricePayed,1);
		uint fairPrice = currentClaimPrice - ((currentClaimPrice * wizardCommissionFractionNum) / wizardCommissionFractionDen);
		
		if (pricePayed == fairPrice){
			kingReceivedHisMoney = true;
		}
	}
	
	
		
	function isItOK() returns (bool ok)
	{
		return kingReceivedHisMoney;
		//return true;
	}
	
		
}
