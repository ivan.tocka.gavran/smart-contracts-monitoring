var KoESpecContract = artifacts.require("./KoEKingsSpecification.sol")
var KingOfEtherContract = artifacts.require("./KingOfTheEtherThrone.sol");



contract('KingOfEtherContract', function(accounts) {
	var setCheckerUsage = true;
	var kingOfEtherInstance;
	
	
	
	function writeBasicData(instance, iteration, kingsName, cheating = false)
	{
		var shouldICheat = cheating;
		var instanceUsed = instance;
		return instanceUsed.currentMonarch.call()
		.then(function(monarch){
			console.log(monarch);
			console.log("current monarch: "+monarch[0]);
			console.log("price paid: "+monarch[2].valueOf());
			return instanceUsed.currentClaimPrice.call();
		})
		.then(function(price){
			currentClaimPrice = price;
			
			console.log("current claim price: "+currentClaimPrice);
			return instanceUsed.claimThrone(kingsName, shouldICheat,  {from: accounts[iteration], to: instanceUsed.address, value: currentClaimPrice, gas:800000});
		})
		.then(function(){
			console.log("after a transaction was sent\n");
			return instanceUsed.currentMonarch.call();
		})
		.then(function(monarch){
			console.log(monarch);
			console.log("current monarch: "+monarch[0]);
			console.log("price paid: "+monarch[2].valueOf());
			console.log("iteration: "+iteration);
			console.log("=========\n\n");
		});
	}

	
//	it("testing creation of contract and initial variables", function(){
//		var kingOfEtherInstance;
//		var currentMonarchAddress;
//		var currentClaimPrice;
//		return KingOfEtherContract.new(setCheckerUsage)
//		.then(function(instance){
//			kingOfEtherInstance = instance;
//			return kingOfEtherInstance.currentMonarch.call();
//		})
//		.then(function(monarch){
//			currentMonarchAddress = monarch[0];
//			return kingOfEtherInstance.currentClaimPrice.call();
//		})
//		.then(function(price){
//			currentClaimPrice = price;
//			console.log("current monarch: "+currentMonarchAddress+"\n");
//			console.log("current price: "+currentClaimPrice+"\n");
//			console.log("==========\n");
//		});
//			
//	});
	
	it("testing iterations", function(){
		var currentMonarchAddress;
		var currentClaimPrice;
		var savedInstance;
		
		
		return KingOfEtherContract.new(setCheckerUsage)
		.then(function(instance){
			savedInstance = instance;
			return writeBasicData(savedInstance, 1, "king");
		})
		.then(function(){
			return writeBasicData(savedInstance, 2, "kingkong");
		})
		.then(function(){
			return writeBasicData(savedInstance, 3, "arthur");
		})
		.then(function(){
			return writeBasicData(savedInstance, 4, "karl");
		});
		
	});
	

	
	it("testing the behavior if KoE contract is trying to cheat", function(){
		var currentMonarchAddress;
		var currentClaimPrice;
		var savedInstance;
		
		
		return KingOfEtherContract.new(setCheckerUsage)
		.then(function(instance){
			savedInstance = instance;
			return writeBasicData(savedInstance, 1, "king", false);
		})
		.then(function(){
			return writeBasicData(savedInstance, 2, "kingkong", false);
		})
		.then(function(){
			return writeBasicData(savedInstance, 3, "arthur", true);
		})
		.then(function(){
			return writeBasicData(savedInstance, 4, "karl");
		});
		
	});
	
  
});


